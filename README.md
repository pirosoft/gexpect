# GExpect #

[ ![Download](https://api.bintray.com/packages/pirosoft/mvn/gexpect/images/download.svg) ](https://bintray.com/pirosoft/mvn/gexpect/_latestVersion) Expectation lib inspired by RSpec - excellent BDD framework for Ruby

### Quick Start / Examples ###

```
expect {
	save(new Book("BDD"))
}.to change {
	bookRepository.findAllByTitle("BDD").count()
}.by(1)
```