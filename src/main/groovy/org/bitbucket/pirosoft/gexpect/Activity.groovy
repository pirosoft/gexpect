package org.bitbucket.pirosoft.gexpect

/**
 * Activity holds code under test ready to be run by GExpect framework and allows you to finish writing GExpect
 * sentence. Usually instances of this class are created by factory methods from GExpect class.
 */
class Activity {

    private Closure closure

    Activity(Closure closure) {
        this.closure = closure
    }

    /**
     * Starts a second half of GExpect sentence
     *
     * @param expectation describes expected behavior of code under test
     */
    void to(Expectation expectation) {
        expectation.onBefore()
        closure.call()
        expectation.onAfter()
    }
}
