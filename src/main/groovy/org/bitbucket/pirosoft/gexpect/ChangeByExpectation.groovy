package org.bitbucket.pirosoft.gexpect

class ChangeByExpectation implements Expectation {

    private Closure measure
    private delta
    private recordedFrom
    private recordedTo
    private expectedRecordedTo

    ChangeByExpectation(Closure measure, delta) {
        this.measure = measure
        this.delta = delta
    }

    @Override
    void onBefore() {
        recordedFrom = measure.call()
        expectedRecordedTo = recordedFrom + delta
    }

    @Override
    void onAfter() {
        recordedTo = measure.call()
        assert recordedTo == expectedRecordedTo
    }
}
