package org.bitbucket.pirosoft.gexpect

class ChangeExpectationBuilder {

    private Closure measure

    ChangeExpectationBuilder(Closure measure) {
        this.measure = measure
    }

    FromToBuilder from(fromValue) {
        new FromToBuilder(fromValue)
    }

    class FromToBuilder {

        private fromValue

        FromToBuilder(fromValue) {
            this.fromValue = fromValue
        }

        ChangeFromToExpectation to(toValue) {
            new ChangeFromToExpectation(measure, fromValue, toValue)
        }
    }

    ChangeByExpectation by(delta) {
        return new ChangeByExpectation(measure, delta)
    }
}
