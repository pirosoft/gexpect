package org.bitbucket.pirosoft.gexpect

class ChangeFromToExpectation implements Expectation {

    private Closure measure
    private expectedFrom
    private expectedTo

    ChangeFromToExpectation(Closure measure, expectedFrom, expectedTo) {
        this.measure = measure
        this.expectedFrom = expectedFrom
        this.expectedTo = expectedTo
    }

    @Override
    void onBefore() {
        def recordedFrom = measure.call()
        assert recordedFrom == expectedFrom
    }

    @Override
    void onAfter() {
        def recordedTo = measure.call()
        assert recordedTo == expectedTo
    }
}
