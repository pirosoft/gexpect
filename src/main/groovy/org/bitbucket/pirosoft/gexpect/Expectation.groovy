package org.bitbucket.pirosoft.gexpect

/**
 * Implement this interface to extend abilities of GExpect library by custom expectations.
 */
interface Expectation {

    /**
     * Called before running code under test
     */
    void onBefore()

    /**
     * Called after running code under test
     */
    void onAfter()
}
