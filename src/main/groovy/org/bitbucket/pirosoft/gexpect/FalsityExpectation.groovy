package org.bitbucket.pirosoft.gexpect

class FalsityExpectation implements Expectation {

    private Closure measure

    FalsityExpectation(Closure measure) {
        this.measure = measure
    }

    @Override
    void onBefore() {
        assert measure()
    }

    @Override
    void onAfter() {
        assert !measure()
    }
}
