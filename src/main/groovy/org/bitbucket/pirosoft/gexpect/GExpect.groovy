package org.bitbucket.pirosoft.gexpect

/**
 * Main class for building expectation sentences inspired by <a href="http://rspec.info">RSpec</a> - excellent BDD framework for Ruby.
 * <p>
 * Basic GExpect sentence is similar to: <br/>
 * <br/>
 * <i>Expect</i> [code-under-test] <i>to</i> [behave-as-expected]
 * </p>
 *
 * <br/>
 * Examples:
 * <pre>
 *     expect { value++ }.to change { value }.by(1)
 *
 *     expect { value++ }.to change { value }.from(1).to(2)
 * </pre>
 */
class GExpect {

    /**
     * Starts first half of GExpect sentence
     *
     * @param activityClosure closure that holds a "code under test"
     * @return activity object
     */
    static Activity expect(Closure activityClosure) {
        new Activity(activityClosure)
    }

    /**
     * Starts building a change expectation. This expectation will measure values that are
     * expected to be changed by code under test. Measurement is performed twice, before and
     * after execution of code under test.
     *
     * @param measure closure for measuring a value
     * @return
     */
    static ChangeExpectationBuilder change(Closure measure) {
        new ChangeExpectationBuilder(measure)
    }

    /**
     * Builds a TruthExpectation. This expectation assumes that measured value is true
     *
     * @param measure closure for measuring a value
     * @return
     */
    static TruthExpectation makeTrue(Closure measure) {
        new TruthExpectation(measure)
    }

    /**
     * Builds a FalsityExpectation. This expectation assumes that measured value is false
     *
     * @param measure closure for measuring a value
     * @return
     */
    static FalsityExpectation makeFalse(Closure measure) {
        new FalsityExpectation(measure)
    }
}
