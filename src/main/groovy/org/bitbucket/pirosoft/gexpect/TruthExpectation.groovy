package org.bitbucket.pirosoft.gexpect

class TruthExpectation implements Expectation {

    private Closure measure

    TruthExpectation(Closure measure) {
        this.measure = measure
    }

    @Override
    void onBefore() {
        assert !measure()
    }

    @Override
    void onAfter() {
        assert measure()
    }
}
