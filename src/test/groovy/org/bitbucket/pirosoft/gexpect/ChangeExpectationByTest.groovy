package org.bitbucket.pirosoft.gexpect

import org.junit.Test

import static groovy.test.GroovyAssert.shouldFail
import static GExpect.change
import static GExpect.expect

class ChangeExpectationByTest {

    @Test
    void "verifies change a value by positive Delta"() {
        def value = 100
        expect { value++ }.to change { value }.by(1)
    }

    @Test
    void "verifies change a value by negative Delta"() {
        def value = 100
        expect { value-- }.to change { value }.by(-1)
    }

    @Test
    void "fails when to changed by different delta"() {
        def value = 100

        def e = shouldFail AssertionError, {
            expect { value = 150 }.to change { value }.by(5)
        }

        assert e.message == '''\
assert recordedTo == expectedRecordedTo
       |          |  |
       150        |  105
                  false'''
    }

    @Test
    void "verifies change a value by zero Delta"() {
        def value = 100
        expect {}.to change { value }.by(0)
    }

    @Test
    void "verifies change on cloned lists by Delta item"() {
        def list = [1, 2, 3]
        expect { list += 4 }.to change { list }.by([4])
    }

    @Test
    void "verifies change the same list object by Delta item"() {
        def list = [1, 2, 3]
        expect { list.add(4) }.to change { list }.by([4])
    }

    @Test
    void "verifies change on list size by Delta"() {
        def list = [1, 2, 3]
        expect { list.add(4) }.to change { list.size() }.by(1)
    }
}
