package org.bitbucket.pirosoft.gexpect

import org.junit.Test

import static groovy.test.GroovyAssert.shouldFail
import static GExpect.change
import static GExpect.expect

class ChangeExpectationFromToTest {

    @Test
    void "verifies change a value from A to B"() {
        def value = 'A'
        expect { value = 'B' }.to change { value }.from('A').to('B')
    }

    @Test
    void "fails when from value is incorrect"() {
        def value = 'A'

        def e = shouldFail AssertionError, {
            expect {}.to change { value }.from('B').to('A')
        }

        assert e.message == '''\
assert recordedFrom == expectedFrom
       |            |  |
       A            |  B
                    false'''
    }

    @Test
    void "fails when to value is incorrect"() {
        def value = 'A'

        def e = shouldFail AssertionError, {
            expect {}.to change { value }.from('A').to('C')
        }

        assert e.message == '''\
assert recordedTo == expectedTo
       |          |  |
       A          |  C
                  false'''
    }

}
