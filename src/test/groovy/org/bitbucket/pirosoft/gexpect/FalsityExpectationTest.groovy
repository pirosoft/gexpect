package org.bitbucket.pirosoft.gexpect

import org.junit.Test

import static groovy.test.GroovyAssert.shouldFail
import static GExpect.*

class FalsityExpectationTest {

    @Test
    void "verifies activity makes measure return false"() {
        def value = true
        expect { value = false }.to makeFalse { value }
    }

    @Test
    void "fails when activity didn't make a measure false"() {
        def value = true

        def e = shouldFail AssertionError, {
            expect {}.to makeFalse { value }
        }

        assert e.message == '''\
assert !measure()
       ||
       |true
       false'''
    }

    @Test
    void "fails when first measure is false"() {
        shouldFail AssertionError, {
            expect {}.to makeTrue { false }
        }
    }

    @Test
    void "Example: verifies if an activity added object to the list"() {
        def list = []
        expect { list.add(1) }.to makeFalse { list.empty }
    }
}