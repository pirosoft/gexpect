package org.bitbucket.pirosoft.gexpect

import org.junit.Test

import static groovy.test.GroovyAssert.shouldFail
import static org.junit.Assert.fail
import static org.mockito.Mockito.*
import static GExpect.expect

class GExpectTest {

    @Test
    void 'runs activity closure'() {
        Expectation pass = [
                onBefore: {},
                onAfter : {}
        ] as Expectation

        boolean executed = false

        expect {
            executed = true
        }.to pass

        assert executed
    }

    @Test
    void 'checks for expectation result'() {
        Expectation expectation = mock Expectation

        expect {}.to expectation
        verify(expectation, times(1)).onBefore()
        verify(expectation, times(1)).onAfter()
    }

    @Test
    void 'allows expectation to fail'() {
        Expectation fail = [
                onBefore: {},
                onAfter : { fail() },
        ] as Expectation

        shouldFail AssertionError, {
            expect {}.to fail
        }
    }

    @Test
    void 'allows expectation to pass'() {
        Expectation pass = [
                onBefore: {},
                onAfter : {}
        ] as Expectation

        expect {}.to pass
    }

    @Test
    void 'allows expectation to measure change'() {
        int i = 100

        int valueBefore = 0
        int valueAfter = 0

        Expectation measure = [
                onBefore: { valueBefore = i },
                onAfter : { valueAfter = i }
        ] as Expectation

        expect { i++ }.to measure
        assert valueBefore == 100
        assert valueAfter == 101
    }
}
