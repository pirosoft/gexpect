package org.bitbucket.pirosoft.gexpect

import org.junit.Test

import static groovy.test.GroovyAssert.shouldFail
import static GExpect.expect
import static GExpect.makeTrue

class TruthExpectationTest {

    @Test
    void "verifies activity makes measure return true"() {
        def value = false
        expect { value = true }.to makeTrue { value }
    }

    @Test
    void "fails when activity didn't make a measure true"() {
        def value = false

        def e = shouldFail AssertionError, {
            expect {}.to makeTrue { value }
        }

        assert e.message == '''\
assert measure()
       |
       false'''
    }

    @Test
    void "fails when first measure is true"() {
        shouldFail AssertionError, {
            expect {}.to makeTrue { true }
        }
    }

    @Test
    void "Example: verifies if an activity added object to the list"() {
        def list = []
        expect { list.add(1) }.to makeTrue { list }
    }
}